﻿using System;

namespace ru.calc.main.Helpers
{
    public class ConsoleResultAttribute : Attribute
    {
        public string [] Tags { get; private set; }

        public ConsoleResultAttribute(params string[] tags)
        {
            Tags = tags;
        }
    }
}