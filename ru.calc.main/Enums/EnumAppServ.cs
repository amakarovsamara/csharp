﻿using System;
using System.Collections.Generic;
using System.Linq;
using ru.calc.main.Helpers;

namespace ru.calc.main.Enums
{
    public static class EnumAppServ
    {
        private static readonly IDictionary<string, ConsoleReadType> Cash;

        static EnumAppServ()
        {
            Cash = new Dictionary<string, ConsoleReadType>();
            var enums = Enum.GetValues(typeof(ConsoleReadType)).Cast<ConsoleReadType>().ToArray();
            foreach (var consoleType in enums)
            {
                var attributes = GetAttribute<ConsoleResultAttribute>(consoleType, consoleType.GetType());

                if (attributes.Any())
                {
                    foreach (var tag in attributes.First().Tags)
                    {
                        var t = tag.Trim().ToLower();
                        if (!Cash.ContainsKey(t))
                            Cash.Add(t, consoleType);
                    }
                }
            }
        }

        public static T [] GetAttribute<T>(object value, Type valueType)
            where T : Attribute
        {
            var fi = valueType.GetField(value.ToString());
            var attributes = (T[])fi.GetCustomAttributes(typeof(T), false);
            return attributes;
        }

        public static ConsoleReadType GetConsoleReadType(this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                input = input.Trim().ToLower();
                if (Cash.ContainsKey(input))
                    return Cash[input];
            }
            return ConsoleReadType.None;
        }

    }
}