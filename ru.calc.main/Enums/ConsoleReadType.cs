﻿using ru.calc.main.Helpers;

namespace ru.calc.main.Enums
{
    public enum ConsoleReadType
    {
        [ConsoleResult]
        None,

        [ConsoleResult("выход", "exit", "quit")]
        Exit,

        [ConsoleResult("add", "new")]
        AddFormula,
    }

}