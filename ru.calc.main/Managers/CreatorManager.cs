﻿using System;
using System.ServiceModel;
using ru.calc.core.Enums;
using ru.calc.main.ServiceReference;

namespace ru.calc.main.Managers
{
    public class CreatorManager : BaseManger
    {
        private CreatorManager()
        {
            
        }

        private static readonly CreatorManager _instance = new CreatorManager();

        public static CreatorManager Instance {get { return _instance; }}

        private readonly IMainService _service = new MainServiceClient();
        
        public int? AddFormula(string formula)
        {
            try
            {
                return _service.AddFormula(formula);
            }
            catch (ServiceActivationException e)
            {
                // Не удается активировать запрошенную службу
            }
            catch (TimeoutException)
            {
                ServerResult(ServerResultType.TimeOutException);
                return null;
            }
            catch (EndpointNotFoundException)
            {
                ServerResult(ServerResultType.EndpointNotFound);
                return null;
            }
            return null;
        }


    }
}