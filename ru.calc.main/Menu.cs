﻿using ru.calc.core.Enums;
using ru.calc.main.Enums;
using ru.calc.main.Managers;
using System;

namespace ru.calc.Main
{
    public static class Menu
    {
        public static void MainMenu()
        {
            while (true)
            {
                var input = Console.ReadLine();
                var consoleInput = input.GetConsoleReadType();

                switch (consoleInput)
                {
                        case ConsoleReadType.Exit:
                        return;

                        case ConsoleReadType.AddFormula:
                        var formula = Console.ReadLine();
                        var result = CreatorManager.Instance.AddFormula(formula);
                        if(result.HasValue)
                            Console.WriteLine((ServerResultType)result.Value);
                        break;
                }
            }
        }
    }
}