﻿using System;
using ru.calc.core.Enums;

namespace ru.calc.server.Exceptions
{
    public class FormulaParseException : Exception
    {
        public ServerResultType Result { get; private set; }

        public FormulaParseException(ServerResultType result)
        {
            Result = result;
        }
    }
}