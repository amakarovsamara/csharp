﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ru.calc.core.Enums;
using ru.calc.core.ServiceInterfaces;
using ru.calc.data.BaseObjects;
using ru.calc.data.DAO;
using ru.calc.server.Exceptions;
using ru.calc.server.FormulaParsers;

namespace ru.calc.server
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы Service1.svc или Service1.svc.cs в обозревателе решений и начните отладку.
    public class MainService : IMainService
    {
        private MainService()
        {
            NHibernateConfiguration.ConfigureDataBase();
            NHibernateConfiguration.RebuiltDataBase();
        }

        public int? AddFormula(string formula)
        {
            var serverResult = ServerResultType.Success;
            try
            {
                Calculator.Calculate(formula);
            }
            catch (FormulaParseException e)
            {
                serverResult = e.Result;
            }
            return (int?) serverResult;
        }

        public int? GetResult(string name, out double result)
        {
            var serverResult = ServerResultType.Success;
            result = double.NaN;
            try
            {
                result = Calculator.GetResult(name);
            }
            catch (FormulaParseException e)
            {
                serverResult = e.Result;
            }
            return (int?)serverResult;
        }
    }
}
