﻿using System.Collections.Generic;
using System.Linq;
using ru.calc.core.Domains;
using ru.calc.core.Enums;
using ru.calc.data.DAO;
using ru.calc.server.Exceptions;

namespace ru.calc.server.FormulaParsers
{
    public static class Calculator
    {
        public static FormulaObject Calculate(string formula)
        {
            var serverResult = ServerResultType.None;
            FormulaObject fo;
            if (FormulaParser.Parse(formula, out fo))
            {
                var fields = MainParser.GetFields(fo.Value);
                var stringForCalculate = fo.Value;
                var formulaDao = new FormulaDAO();
                if (fields.Any())
                {
                    if(fields.Any(x=>x==fo.Name))
                        throw new FormulaParseException(ServerResultType.FieldNameReserved);
                    
                    var dic = new Dictionary<string, double>();
                    foreach (var field in fields)
                    {
                        var f = formulaDao.GetByName(field);
                        if (f != null && f.Result.HasValue)
                        {
                            dic.Add(field, f.Result.Value);
                        }
                        else
                        {
                            serverResult = ServerResultType.OtherFormulaNotCalculate;
                            dic = null;
                            break;
                        }
                    }
                    if (dic!=null)
                    {
                        stringForCalculate = fo.Value.ReplaceFieldsByDigit(dic);
                    }
                }
                fo.Result = FormulaParser.Calculate(stringForCalculate);
                fo = formulaDao.SaveOrUpdate(fo);
                fo.References = new List<FormulaReferenceObject>(fields.Select(x=>new FormulaReferenceObject(fo, x)));
                fo = formulaDao.SaveOrUpdate(fo);
                if (fo == null)
                {
                    serverResult = ServerResultType.DataBaseError;
                }
            }
            if (serverResult != ServerResultType.None)
            {
                throw new FormulaParseException(serverResult);
            }
            return fo;
        }

        public static double GetResult(string formulaName)
        {
            var dao = new FormulaDAO();
            var fo = dao.GetByName(formulaName);
            if (fo == null)
                throw new FormulaParseException(ServerResultType.FormulaNotFound);
            if (fo.Result == null)
                throw new FormulaParseException(ServerResultType.ResultNotCalculated);

            return fo.Result.Value;
        }
    }
}