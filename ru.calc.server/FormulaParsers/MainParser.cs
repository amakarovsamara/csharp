﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ru.calc.server.FormulaParsers
{
    public static class MainParser
    {
        const string Pattern = @"[a-zA-Zа-яА-Я_]{1}\w*";

        public static bool IsField(this string str)
        {
            str = str.Trim();
            const int maxFieldLength = 250;
            if (!string.IsNullOrEmpty(str) && str.Length<=maxFieldLength)
            {
                return (char.IsLetter(str[0]) || str[0]=='_')
                    && (str.Substring(1).All(x=>char.IsLetterOrDigit(x) || x=='_'));
            }
            return false;
        }

        public static double ? IsDigit(this string str)
        {
            var sign = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            str = str.Trim().Replace(".", sign).Replace(",", sign);
            double res;
            if (double.TryParse(str, out res))
                return res;

            return null;
        }

        public static string[] GetFields(string s)
        {
            var result = Regex.Matches(s, Pattern).Cast<Match>().Select(x => x.Value).Distinct().ToArray();
            return result;
        }

        public static string ReplaceFieldsByDigit(this string s, double replacebleValue)
        {
            return Regex.Replace(s, Pattern, replacebleValue.ToString(CultureInfo.InvariantCulture));
        }

        public static string ReplaceFieldsByDigit(this string s, IEnumerable<KeyValuePair<string, double>> replacebleValues)
        {
            foreach (var replacebleValue in replacebleValues)
            {
                s = s.Replace(replacebleValue.Key, replacebleValue.Value.ToString(CultureInfo.InvariantCulture));
            }
            return s;
        }
    }
}