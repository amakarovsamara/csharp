﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ru.calc.core.Domains;
using ru.calc.core.Enums;
using ru.calc.server.Exceptions;

namespace ru.calc.server.FormulaParsers
{
    public static class FormulaParser
    {
        public static bool Parse(string s, out FormulaObject formulaObject)
        {
            if (s.Count(x => x == '=') != 1)
                throw new FormulaParseException(ServerResultType.AssignmentError);

            var header = s.Substring(0, s.IndexOf('=')).Trim();
            if(!header.IsField())
                throw new FormulaParseException(ServerResultType.HeaderError);

            if(s.Count(x=>x=='(')!=s.Count(x=>x==')'))
                throw new FormulaParseException(ServerResultType.BracketError);

            var formulaOriginal = s.Substring(s.IndexOf('=')+1).Replace(" ", string.Empty);
            var formula = formulaOriginal.ReplaceFieldsByDigit(1);

            Calculate(formula);
            formulaObject = new FormulaObject { Name = header, Value = formulaOriginal };
            return true;
        }


        public static double Calculate(string input)
        {
            // Калькулятор на основе Обратной Польской записи
            string output = GetExpression(input); //Преобразовываем выражение в постфиксную запись
            double result = Counting(output); //Решаем полученное выражение
            return result; //Возвращаем результат
        }

        static private bool IsDelimeter(char c)
        {
            if ((" =".IndexOf(c) != -1))
                return true;
            return false;
        }

        static private bool IsOperator(char с)
        {
            if (("+-/*^()".IndexOf(с) != -1))
                return true;
            return false;
        }

        static private byte GetPriority(char s)
        {
            switch (s)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                case '^': return 5;
                default: return 6;
            }
        }

        static private string GetExpression(string input)
        {
            string output = string.Empty; //Строка для хранения выражения
            var operStack = new Stack<char>(); //Стек для хранения операторов

            for (int i = 0; i < input.Length; i++) //Для каждого символа в входной строке
            {
                //Разделители пропускаем
                if (IsDelimeter(input[i]))
                    continue; //Переходим к следующему символу

                //Если символ - цифра, то считываем все число
                var ch = input[i];
                var sign = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                var digitSign = ("+-".Contains(input[i]) && ( (i>0 && (!char.IsLetterOrDigit(input[i-1]) && input[i-1]=='('))));
                if (Char.IsDigit(input[i]) || input[i] == sign[0] || digitSign) //Если цифра
                {
                    if (digitSign)
                    {
                        output += " ";
                        output += input[i++];
                    }
                    //Читаем до разделителя или оператора, что бы получить число
                    while (!IsDelimeter(input[i]) && !IsOperator(input[i]))
                    {
                        output += input[i]; //Добавляем каждую цифру числа к нашей строке
                        i++; //Переходим к следующему символу

                        if (i == input.Length) break; //Если символ - последний, то выходим из цикла
                    }

                    output += " "; //Дописываем после числа пробел в строку с выражением
                    i--; //Возвращаемся на один символ назад, к символу перед разделителем
                }

                //Если символ - оператор
                if (IsOperator(input[i])) //Если оператор
                {
                    if (input[i] == '(') //Если символ - открывающая скобка
                        operStack.Push(input[i]); //Записываем её в стек
                    else if (input[i] == ')') //Если символ - закрывающая скобка
                    {
                        //Выписываем все операторы до открывающей скобки в строку
                        char s = operStack.Pop();

                        while (s != '(')
                        {
                            output += s.ToString() + ' ';
                            s = operStack.Pop();
                        }
                    }
                    else //Если любой другой оператор
                    {
                        if (operStack.Count > 0) //Если в стеке есть элементы
                            if (GetPriority(input[i]) <= GetPriority(operStack.Peek())) //И если приоритет нашего оператора меньше или равен приоритету оператора на вершине стека
                                output += operStack.Pop().ToString() + " "; //То добавляем последний оператор из стека в строку с выражением

                        operStack.Push(char.Parse(input[i].ToString())); //Если стек пуст, или же приоритет оператора выше - добавляем операторов на вершину стека

                    }
                }
            }

            //Когда прошли по всем символам, выкидываем из стека все оставшиеся там операторы в строку
            while (operStack.Count > 0)
                output += operStack.Pop() + " ";

            return output; //Возвращаем выражение в постфиксной записи
        }

        static private double Counting(string input)
        {
            var elements = input.Split(' ').Where(x=>!string.IsNullOrWhiteSpace(x)).ToArray();
            var operands = elements.Where(x => "*-/+^".Contains(x)).ToArray();
            var fields = elements.Where(x => !operands.Contains(x)).ToArray();


            if(fields.Length-operands.Length!=1) // проверяем соответствие "количество чесел" = "знаков операции"+1 
                throw new FormulaParseException(ServerResultType.InvalidOperation);

            double result = 0; //Результат
            var temp = new Stack<double>(); //Временный стек для решения

            foreach (string elem in elements)
            {
                //Если символ - цифра, то читаем все число и записываем на вершину стека
                var isDigit = elem.IsDigit();
                if (isDigit.HasValue)
                {
                    temp.Push(isDigit.Value); //Записываем в стек
                }
                else if (elem.Length == 1 && IsOperator(elem[0])) //Если символ - оператор
                {
                    //Берем два последних значения из стека
                    if(temp.Count<2)
                        throw new FormulaParseException(ServerResultType.InvalidOperation);
                    double a = temp.Pop();
                    double b = temp.Pop();

                    switch (elem[0]) //И производим над ними действие, согласно оператору
                    {
                        case '+': result = b + a; break;
                        case '-': result = b - a; break;
                        case '*': result = b * a; break;
                        case '/':
                            if(Math.Abs(a)<double.Epsilon)
                                throw new FormulaParseException(ServerResultType.ZeroDivided);
                            result = b / a; break;
                        case '^': result = double.Parse(Math.Pow(double.Parse(b.ToString()), double.Parse(a.ToString())).ToString()); break;
                    }
                    temp.Push(result); //Результат вычисления записываем обратно в стек
                }
            }
            return temp.Peek(); //Забираем результат всех вычислений из стека и возвращаем его
        }
    }
}