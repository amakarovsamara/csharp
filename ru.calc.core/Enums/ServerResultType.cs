﻿using System;
using System.ComponentModel;

namespace ru.calc.core.Enums
{
    public enum ServerResultType : int
    {
        [Description]
        None = 0,

        [Description("Операция успешно выполнена")]
        Success,

        [Description("Попытка присвоить зарезервированное имя")]
        ErrorSystemName,

        [Description("Истекло время ожидания ответа от сервера")]
        TimeOutException,

        [Description("Не удалось подключиться к серверу")]
        EndpointNotFound,

        [Description("Отсутствует присвоение в формуле")]
        AssignmentError,

        [Description("Ошибочное именование переменной")]
        FieldError,

        [Description("Ошибочный заголовок формулы")]
        HeaderError,

        [Description("Отсутствует открывающая (закрывающая) скобка")]
        BracketError,

        [Description("Попытка деления на ноль")]
        ZeroDivided,

        [Description("Попытка выполнить недопустимую операцию")]
        InvalidOperation,

        [Description("Невозможно вычислить формулу. Имеются ранее нерасчитанные и отсутствующие формулы")]
        OtherFormulaNotCalculate,

        [Description("Формула с указанным именованием уже существует")]
        FieldNameReserved,

        [Description("Ошибка обращения к базе данных")]
        DataBaseError,

        [Description("Запрашиваемая формула не найдена")]
        FormulaNotFound,

        [Description("Значение формулы не вычислено")]
        ResultNotCalculated
    }
}