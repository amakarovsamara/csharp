﻿using System.Collections.Generic;

namespace ru.calc.core.Domains
{
    public class FormulaReferenceObject : DomainObject<long>
    {
        public FormulaReferenceObject(){}

        public FormulaReferenceObject(FormulaObject parent, string relationFormula)
        {
            Parent = parent;
            RelationFormula = relationFormula;
        }

        public FormulaObject Parent { get; set; }

        public string RelationFormula { get; set; }

        public override int GetHashCode()
        {
            var hash = 0;
            if (Parent != null)
                hash ^= Parent.GetHashCode();
            if (RelationFormula != null)
                hash ^= RelationFormula.GetHashCode();

            return hash;
        }
    }
}