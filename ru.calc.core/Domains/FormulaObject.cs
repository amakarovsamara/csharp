﻿using System.Collections.Generic;
using System.Globalization;

namespace ru.calc.core.Domains
{
    public class FormulaObject : DomainObject<long>
    {
        public  string Name { get; set; }

        public string Value { get; set; }

        public double ? Result { get; set; }

        public List<FormulaReferenceObject> References { get; set; } 

        public override int GetHashCode()
        {
            var hash = 0;
            if (Value != null)
                hash ^= Value.GetHashCode();
            if (Name != null)
                hash ^= Name.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return string.Concat(Name, " = ", Value, " | ", Result == null ? "расчет не производился" : Result.Value.ToString(CultureInfo.InvariantCulture));
        }
    }
}