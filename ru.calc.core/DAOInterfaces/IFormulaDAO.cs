﻿using System.Collections.Generic;
using ru.calc.core.Domains;

namespace ru.calc.core.DaoInterfaces
{
    public interface IFormulaDAO : IDAO<FormulaObject, long>
    {
        FormulaObject GetByName(string name);

        List<FormulaObject> GetNotCalculatedFormulas(string name);
    }
}