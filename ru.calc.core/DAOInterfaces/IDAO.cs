﻿using System;
using System.Collections.Generic;

namespace ru.calc.core.DaoInterfaces
{
    /// <summary>
    /// Интерфейс доступа к данным.
    /// </summary>
    /// <typeparam name="T">Тип данных.</typeparam>
    /// <typeparam name="IdT">Тип идентификатора данных.</typeparam>
    public interface IDAO<T, IdT>
    {
        /// <summary>
        /// Fully qualified path of the session factory's config file.
        /// </summary>
        string SessionConfigPath { get; }
        
        /// <summary>
        /// Loads an instance of type T from the DB based on its ID.
        /// </summary>
        T GetByID(IdT id, bool shouldLock);

        /// <summary>
        /// Loads every instance of the requested type with no filtering.
        /// </summary>
        List<T> GetAll();

        /// <summary>
        /// Получает число элементов в БД.
        /// </summary>
        /// <returns>Число элементов текущего типа в БД.</returns>
        int GetCount();

        /// <summary>
        /// Получает число элементов в БД, удовлетворяющих заданному условию.
        /// </summary>
        /// <param name="predicate">Условие фильтрации.</param>
        /// <returns>Число элементов в БД, удовлетворяющих заданному условию</returns>
        int GetCount(Func<T, bool> predicate);

        /// <summary>
        /// Получает сущности по подобной сущности.
        /// </summary>
        /// <param name="exampleInstance">Подобная сущность.</param>
        /// <param name="propertiesToExclude">Свойства сущности, которые не брать в рассмотрение.</param>
        /// <returns>Список похожих сущностей.</returns>
        List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude);

        /// <summary>
        /// Looks for a single instance using the example provided.
        /// </summary>
        /// <exception cref="NonUniqueResultException" />
        T GetUniqueByExample(T exampleInstance, params string[] propertiesToExclude);

        /// <summary>
        /// For entities that have assigned ID's, you must explicitly call Save to add a new one.
        /// See http://www.hibernate.org/hib_docs/reference/en/html/mapping.html#mapping-declaration-id-assigned.
        /// </summary>
        T Save(T entity);

        /// <summary>
        /// For entities with automatatically generated IDs, such as identity, SaveOrUpdate may 
        /// be called when saving a new entity.  SaveOrUpdate can also be called to _update_ any 
        /// entity, even if its ID is assigned.
        /// </summary>
        T SaveOrUpdate(T entity);

        /// <summary>
        /// Удаляет массив данных.
        /// </summary>
        /// <param name="entities">Массив данных для удаления.</param>
        void DeleteCollection(IEnumerable<T> entities);

        /// <summary>
        /// Удаляет сущность.
        /// </summary>
        /// <param name="entity">Сущность для удаления.</param>
        void Delete(T entity);

        /// <summary>
        /// Выполняет сохранение списка объектов в пакетном режиме с помощью транзакций.
        /// </summary>
        /// <param name="domains">Список объектов для сохранения.</param>
        /// <param name="maxCount">Количество объектов в одном пакете.</param>
        void SaveList(IList<T> domains, int maxCount = 100);

        /// <summary>
        /// Возвращает первый элемент, удовлетворяющий условию, или null, если такового нет.
        /// </summary>
        /// <param name="predicate">Условие.</param>
        /// <returns>Элемент.</returns>
        T FirstOrDefault(Func<T, bool> predicate);

        /// <summary>
        /// Получает список объектов по условию.
        /// </summary>
        /// <param name="predicate">Условие.</param>
        /// <returns>Список объектов.</returns>
        IList<T> List(Func<T, bool> predicate);

        /// <summary>
        /// Commits changes regardless of whether there's an open transaction or not.
        /// Сохраняет изменения. Применяет транзакцию.
        /// </summary>
        [Obsolete]
        void CommitChanges();

        /// <summary>
        /// Создает схему базы данных.
        /// </summary>
        void UpdateSchema();

        /// <summary>
        /// Проверяет наличие объекта с заданным id.
        /// </summary>
        /// <param name="id">id объекта.</param>
        /// <returns></returns>
        bool Any(IdT id);
    }
}