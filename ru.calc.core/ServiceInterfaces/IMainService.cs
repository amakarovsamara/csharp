﻿using System.ServiceModel;

namespace ru.calc.core.ServiceInterfaces
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService1" в коде и файле конфигурации.
    [ServiceContract]
    public interface IMainService
    {
        [OperationContract]
        int ? AddFormula(string formula);

        int? GetResult(string name, out double result);
    }
}
