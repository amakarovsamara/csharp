﻿using System;
using System.IO;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Environment = System.Environment;

namespace ru.calc.data.BaseObjects
{
    public static class NHibernateConfiguration
    {
        /// <summary>
        /// Путь к файлу конфигурации базы данных.
        /// </summary>
        public static string PathToConfigurationFile { get; private set; }

        /// <summary>
        /// Перестраивает базу данных.
        /// </summary>
        public static void RebuiltDataBase()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);


            var cfg = new Configuration();
            if (string.IsNullOrEmpty(PathToConfigurationFile))
                cfg.Configure();
            else cfg.Configure(PathToConfigurationFile);

            try
            {
                new SchemaExport(cfg).Drop(false, true);
                new SchemaExport(cfg).Create(true, true);
            }
            catch
            {
                try
                {
                    new SchemaExport(cfg).Drop(false, true);
                    new SchemaExport(cfg).Create(false, true);
                }
                catch (Exception e)
                {
                    throw new Exception("Не удается перестроить схему базы данных.", e);
                }
            }
        }

        public static void ConfigureDataBase()
        {
            // Сохраняем файл с конфигурацией в текущую папку.
            // Файл хранится в ресурсах.
            
            const string dbConfigPath = "NHibernate_SQLite.config";
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                       Path.DirectorySeparatorChar + dbConfigPath;
            var sw = new StreamWriter(path, false);
            sw.Write(Properties.Resources.NHibernate_SQLite);
            sw.Close();
            PathToConfigurationFile = path;
        }
    }
}