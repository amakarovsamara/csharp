﻿using System.Linq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Tool.hbm2ddl;
using ru.calc.data.BaseObjects.NHibernateSessionManagement;
using System;
using System.Collections.Generic;

namespace ru.calc.data.BaseObjects
{
    /// <summary>
    /// Базовый класс доступа к данным, использующий Nhibernate.
    /// </summary>
    /// <typeparam name="T">Тип данных.</typeparam>
    /// <typeparam name="IdT">Тип идентификатора данных.</typeparam>
    public class AbstractNHibernateDAO<T, IdT> : IDAO<T, IdT>
        
    {
        #region Поля, свойства

        private readonly Type _persitentType = typeof (T);
        private readonly string _sessionFactoryConfigPath;

        /// <summary>
        /// Fully qualified path of the session factory's config file.
        /// </summary>
        public string SessionConfigPath
        {
            get { return _sessionFactoryConfigPath; }
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Создает класс доступа к данным, использующий Nhibernate.
        /// </summary>
        protected AbstractNHibernateDAO()
        {
        }

        /// <summary>
        /// Создает класс доступа к данным, использующий Nhibernate.
        /// </summary>
        /// <param name="sessionFactoryConfigPath">Fully qualified path of the session factory's config file.</param>
        public AbstractNHibernateDAO(string sessionFactoryConfigPath)
        {
            if (string.IsNullOrEmpty(sessionFactoryConfigPath))
                throw new ArgumentException("Parameter sessionFactoryConfigPath is null or empty.",
                                            "sessionFactoryConfigPath");

            _sessionFactoryConfigPath = sessionFactoryConfigPath;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Exposes the ISession used within the DAO.
        /// </summary>
        protected ISession NHibernateSession
        {
            get { return NHibernateSessionManager.Instance.GetSessionFrom(_sessionFactoryConfigPath); }
        }


        /// <summary>
        /// Создает схему базы данных.
        /// </summary>
        public void UpdateSchema()
        {
            var cfg = new Configuration();
            cfg.Configure(_sessionFactoryConfigPath);

            try
            {
                CreateSchema(cfg);
            }
            catch (Exception)
            {
                DropSchema(cfg);
                CreateSchema(cfg);
            }
        }

        public virtual bool Any(IdT id)
        {
            try { return GetByID(id, false) != null; }
            catch { return false; }
        }

        /// <summary>
        /// Loads an instance of type T from the DB based on its ID.
        /// </summary>
        public T GetByID(IdT id, bool shouldLock)
        {
            T entity;

            using (ISession session = NHibernateSession)
            {
                if (shouldLock)
                {
                    entity = (T) session.Load(_persitentType, id, LockMode.Upgrade);
                }
                else
                {
                    entity = (T) session.Load(_persitentType, id);
                }
            }

            return entity;
        }

        /// <summary>
        /// Loads every instance of the requested type with no filtering.
        /// </summary>
        public List<T> GetAll()
        {
            return GetByCriteria();
        }

        /// <summary>
        /// Получает число элементов в БД.
        /// </summary>
        /// <returns>Число элементов текущего типа в БД.</returns>
        public int GetCount()
        {
            return GetCount(x => true);
        }

        /// <summary>
        /// Получает число элементов в БД, удовлетворяющих заданному условию.
        /// </summary>
        /// <param name="predicate">Условие фильтрации.</param>
        /// <returns>Число элементов в БД, удовлетворяющих заданному условию</returns>
        public int GetCount(Func<T, bool> predicate)
        {
            using (ISession session = NHibernateSession)
            {
                return session.Query<T>().Count(predicate);
            }
        }

        /// <summary>
        /// Loads every instance of the requested type using the supplied <see cref="ICriterion" />.
        /// If no <see cref="ICriterion" /> is supplied, this behaves like <see cref="IDAO{T,IdT}.GetAll" />.
        /// </summary>
        public List<T> GetByCriteria(params ICriterion[] criterion)
        {
            using (ISession session = NHibernateSession)
            {
                ICriteria criteria = session.CreateCriteria(_persitentType);

                foreach (ICriterion criterium in criterion)
                {
                    criteria.Add(criterium);
                }

                return criteria.List<T>() as List<T>;
            }
        }

        /// <summary>
        /// Получает сущности по подобной сущности.
        /// </summary>
        /// <param name="exampleInstance">Подобная сущность.</param>
        /// <param name="propertiesToExclude">Свойства сущности, которые не брать в рассмотрение.</param>
        /// <returns>Список похожих сущностей.</returns>
        public List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude)
        {
            using (ISession session = NHibernateSession)
            {
                ICriteria criteria = session.CreateCriteria(_persitentType);
                Example example = Example.Create(exampleInstance);

                foreach (string propertyToExclude in propertiesToExclude)
                {
                    example.ExcludeProperty(propertyToExclude);
                }

                criteria.Add(example);

                return criteria.List<T>() as List<T>;
            }
        }

        /// <summary>
        /// Looks for a single instance using the example provided.
        /// </summary>
        /// <exception cref="NonUniqueResultException" />
        public T GetUniqueByExample(T exampleInstance, params string[] propertiesToExclude)
        {
            List<T> foundList = GetByExample(exampleInstance, propertiesToExclude);

            if (foundList.Count > 1)
            {
                throw new NonUniqueResultException(foundList.Count);
            }

            if (foundList.Count > 0)
            {
                return foundList[0];
            }

            return default(T);
        }

        /// <summary>
        /// For entities that have assigned ID's, you must explicitly call Save to add a new one.
        /// See http://www.hibernate.org/hib_docs/reference/en/html/mapping.html#mapping-declaration-id-assigned.
        /// </summary>
        public virtual T Save(T entity)
        {
            using (ISession session = NHibernateSession)
            {
                using (ITransaction t = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    t.Commit();
                    return entity;
                }
            }
        }

        /// <summary>
        /// For entities with automatatically generated IDs, such as identity, SaveOrUpdate may 
        /// be called when saving a new entity.  SaveOrUpdate can also be called to _update_ any 
        /// entity, even if its ID is assigned.
        /// </summary>
        public T SaveOrUpdate(T entity)
        {
            return Save(entity);
        }

        /// <summary>
        /// Удаляет сущность.
        /// </summary>
        /// <param name="entity">Сущность для удаления.</param>
        public virtual void Delete(T entity)
        {
            using (ISession session = NHibernateSession)
            {
                using (ITransaction t = session.BeginTransaction())
                {
                    session.Delete(entity);
                    t.Commit();
                }
            }
        }

        /// <summary>
        /// Удаляет массив данных.
        /// </summary>
        /// <param name="entities">Массив данных для удаления.</param>
        public virtual void DeleteCollection(IEnumerable<T> entities)
        {
            using (ISession session = NHibernateSession)
            {
                using (ITransaction t = session.BeginTransaction())
                {
                    foreach (T entity in entities)
                    {
                        session.Delete(entity);
                    }
                    t.Commit();
                }
            }
        }

        /// <summary>
        /// Выполняет сохранение списка объектов в пакетном режиме с помощью транзакций.
        /// </summary>
        /// <param name="domains">Список объектов для сохранения.</param>
        /// <param name="maxCount">Количество объектов в одном пакете.</param>
        public virtual void SaveList(IList<T> domains, int maxCount = 100)
        {
            using (ISession session = NHibernateSession)
            {
                // Цикл по всем объектам в списке.
                int i = 0;
                while (i < domains.Count)
                {
                    // Создание транзакции.
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        for (int j = 0; j < maxCount; j++)
                        {
                            // Добавление объекта в сессию.
                            if (i + j < domains.Count)
                            {
                                session.SaveOrUpdate(domains[i + j]);
                            }
                        }
                        // Транзакция в базу.
                        transaction.Commit();
                    }
                    for (int j = 0; j < maxCount; j++)
                    {
                        // Удаление объекта из сессии.
                        if (i + j < domains.Count)
                        {
                            session.Evict(domains[i + j]);
                        }
                    }
                    i = i + maxCount;
                }
            }
        }

        /// <summary>
        /// Возвращает первый элемент, удовлетворяющий условию, или null, если такового нет.
        /// </summary>
        /// <param name="predicate">Условие.</param>
        /// <returns>Элемент.</returns>
        public T FirstOrDefault(Func<T, bool> predicate)
        {
            using (ISession session = NHibernateSession)
            {
                return session.Query<T>().FirstOrDefault(predicate);
            }
        }

        /// <summary>
        /// Получает список объектов по условию.
        /// </summary>
        /// <param name="predicate">Условие.</param>
        /// <returns>Список объектов.</returns>
        public virtual IList<T> List(Func<T, bool> predicate)
        {
            using (ISession session = NHibernateSession)
            {
                return session.Query<T>().Where(predicate).ToList();
            }
        }

        /// <summary>
        /// Commits changes regardless of whether there's an open transaction or not.
        /// Сохраняет изменения. Применяет транзакцию.
        /// </summary>
        [Obsolete]
        public void CommitChanges()
        {
        }

        private void CreateSchema(Configuration cfg)
        {
            new SchemaExport(cfg).Create(true, true);
        }

        private void DropSchema(Configuration cfg)
        {
            new SchemaExport(cfg).Drop(true, true);
        }

        #endregion
    }
}