﻿using System;
using System.Collections;
using NHibernate;
using NHibernate.Cfg;

namespace ru.calc.data.BaseObjects.NHibernateSessionManagement
{
    /// <summary>
    /// Handles creation and management of sessions and transactions.  It is a singleton because 
    /// building the initial session factory is very expensive. Inspiration for this class came 
    /// from Chapter 8 of Hibernate in Action by Bauer and King.  Although it is a sealed singleton
    /// you can use TypeMock (http://www.typemock.com) for more flexible testing.
    /// </summary>
    public sealed class NHibernateSessionManager
    {
        #region Thread-safe, lazy Singleton

        /// <summary>
        /// Private constructor to enforce singleton.
        /// </summary>
        private NHibernateSessionManager()
        {
        }

        /// <summary>
        /// This is a thread-safe, lazy singleton.  See http://www.yoda.arachsys.com/csharp/singleton.html
        /// for more details about its implementation.
        /// </summary>
        public static NHibernateSessionManager Instance
        {
            get { return Nested.NHibernateSessionManager; }
        }

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton.
        /// </summary>
        private class Nested
        {
            internal static readonly NHibernateSessionManager NHibernateSessionManager =
                new NHibernateSessionManager();
        }

        #endregion
        private readonly Hashtable _sessionFactories = new Hashtable();
        private readonly object _syncRoot = new object();

        /// <summary>
        /// This method attempts to find a session factory stored in <see cref="_sessionFactories" />
        /// via its name; if it can't be found it creates a new one and adds it the hashtable.
        /// </summary>
        /// <param name="sessionFactoryConfigPath">Path location of the factory config</param>
        private ISessionFactory GetSessionFactoryFor(string sessionFactoryConfigPath)
        {
            lock (_syncRoot)
            {
                //  Attempt to retrieve a stored SessionFactory from the hashtable.
                var sessionFactory = (ISessionFactory) _sessionFactories[sessionFactoryConfigPath];

                //  Failed to find a matching SessionFactory so make a new one.
                if (sessionFactory == null)
                {
                    var cfg = new Configuration();
                    cfg.Configure(sessionFactoryConfigPath);
                    
                    //  Now that we have our Configuration object, create a new SessionFactory
                    sessionFactory = cfg.BuildSessionFactory();

                    if (sessionFactory == null)
                    {
                        throw new InvalidOperationException("cfg.BuildSessionFactory() returned null.");
                    }

                    _sessionFactories.Add(sessionFactoryConfigPath, sessionFactory);
                }

                return sessionFactory;
            }
        }

        public ISession GetSessionFrom(string sessionFactoryConfigPath)
        {
            if (string.IsNullOrEmpty(sessionFactoryConfigPath))
                throw new ArgumentException("Parameter sessionFactoryConfigPath is null or empty.",
                                            "sessionFactoryConfigPath");
            ISession session = GetSessionFactoryFor(sessionFactoryConfigPath).OpenSession();

            if (session == null) throw new Exception("Session is null.");

            return session;
        }

    }
}