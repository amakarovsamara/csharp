﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using ru.calc.core.DaoInterfaces;
using ru.calc.core.Domains;
using ru.calc.data.BaseObjects;

namespace ru.calc.data.DAO
{
    public class FormulaDAO : AbstractNHibernateDAO<FormulaObject, long>, IFormulaDAO
    {
        /// <summary>
        /// Создает объект, обеспечивающий доступ к данным формул.
        /// </summary>
        public FormulaDAO()
            : base(NHibernateConfiguration.PathToConfigurationFile)
        {
        }

        public FormulaObject GetByName(string name)
        {
            using (ISession session = NHibernateSession)
            {
                var fo =session.Query<FormulaObject>().SingleOrDefault(x => x.Name == name);
                if (fo != null)
                {
                     fo.References = session.Query<FormulaReferenceObject>().Where(x => x.Parent == fo).ToList();
                }
                return fo;
            }
        }

        public List<FormulaObject> GetNotCalculatedFormulas(string name)
        {
            using (ISession session = NHibernateSession)
            {
                return session.Query<FormulaReferenceObject>().Where(x=>x.RelationFormula == name).Select(t=>t.Parent).Distinct().ToList();
            }
        }

        public new FormulaObject SaveOrUpdate(FormulaObject entity)
        {
            if (entity.References != null && entity.References.Any())
            {
                var daoRef = new FormulaReferenceDAO();
                daoRef.SaveList(entity.References);
            }
            base.SaveOrUpdate(entity);
            return entity;
        }
    }
}