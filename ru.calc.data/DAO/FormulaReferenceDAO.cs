﻿using System.Collections.Generic;
using ru.calc.core.Domains;
using ru.calc.data.BaseObjects;

namespace ru.calc.data.DAO
{
    public class FormulaReferenceDAO : AbstractNHibernateDAO<FormulaReferenceObject, long>
    {
         /// <summary>
        /// Создает объект, обеспечивающий доступ к данным формул.
        /// </summary>
        public FormulaReferenceDAO()
            : base(NHibernateConfiguration.PathToConfigurationFile)
        {
        }
    }
}