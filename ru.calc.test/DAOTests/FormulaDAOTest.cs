﻿using NUnit.Framework;
using ru.calc.core.Domains;
using ru.calc.data.BaseObjects;
using ru.calc.data.DAO;

namespace ru.calc.test.DAOTests
{
    [TestFixture]
    public class FormulaDAOTest
    {
        [Test]
        public void RebuildDataBase()
        {
            NHibernateConfiguration.ConfigureDataBase();
            NHibernateConfiguration.RebuiltDataBase();

            var formula = new FormulaObject()
            {
                Name = "A",
                Value = "5*5-9"
            };
            var dao = new FormulaDAO();
            var result = dao.SaveOrUpdate(formula);
            Assert.IsTrue(!result.IsTransient(), "ID должен отличаться от 0");
            Assert.AreEqual(formula.GetHashCode(), result.GetHashCode(), "Элемент из БД отличается от несохраненного");
        }
    }
}