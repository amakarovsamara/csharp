﻿using System.Text;
using NUnit.Framework;
using ru.calc.server.FormulaParsers;

namespace ru.calc.test.ParserTests
{
    public class MainParserTest
    {
        [Test]
        public void ParserFieldTest()
        {
            string [] success =
            {
                "_", "dge4534", "_45re43", "dfrtedg"
            };

            var longField = new StringBuilder();
            for (int i = 0; i < 300; i++)
            {
                longField.Append('q');
            }
            string[] fail =
            {
                "234", "_.3253", "rg fge", "^$5gfd", longField.ToString(),

            };
            foreach (var s in success)
            {
                Assert.IsTrue(s.IsField(), string.Concat(s, " не является допустимым значением"));
            }
            foreach (var s in fail)
            {
                Assert.IsFalse(s.IsField(), string.Concat(s, " не является допустимым значением"));
            }
        }

        [Test]
        public void ParserDigitTest()
        {
            string[] digits =
            {
                "45", "-978", "5.34534", "1.052033E+003", "43543,4534"
            };
            foreach (var digit in digits)
            {
                Assert.IsTrue(digit.IsDigit().HasValue, "Не удается преобразовать число "+digit);
            }

        }
    }
}