﻿using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using ru.calc.core.Domains;
using ru.calc.core.Enums;
using ru.calc.data.BaseObjects;
using ru.calc.data.DAO;
using ru.calc.main.Enums;
using ru.calc.server.Exceptions;
using ru.calc.server.FormulaParsers;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;

namespace ru.calc.test.ParserTests
{
    public class ParserTest
    {
        [Test]
        public void FormulaTest()
        {
            const string success = "A = (56.8*(-85/sdfg)+(-rtrt)+89/yyy-yyy+прокен+_323ываfgfd)";
            try
            {
                FormulaObject fo;
                if (FormulaParser.Parse(success, out fo))
                {
                    Trace.WriteLine(string.Concat(fo.ToString(), " | Формула прошла валидацию"));
                    
                }
            }
            catch (FormulaParseException e)
            {
                var errorMsg = EnumAppServ.GetAttribute<DescriptionAttribute>(e.Result, typeof(ServerResultType)).First().Description;
                Assert.That(false, string.Concat(success, " | ",errorMsg));
            }
        }

        [Test]
        public void CalculateTest()
        {
            const string success = "A = (56.8*(-85/sdfg)+(-rtrt)+89/yyy-yyy+прокен+_323ываfgfd)";
            var t = "A = 5 + 2";
            var t2 = "R = A * 2";
            try
            {
                NHibernateConfiguration.ConfigureDataBase();
                NHibernateConfiguration.RebuiltDataBase();

                FormulaObject fo;
                if (FormulaParser.Parse(t, out fo))
                {
                    fo = Calculator.Calculate(t);
                    if (FormulaParser.Parse(t2, out fo))
                        fo = Calculator.Calculate(t2);
                    Trace.WriteLine(string.Concat(fo.ToString(), " | Формула прошла валидацию"));
                    var dao = new FormulaDAO();
                    
                    var g = dao.GetByName("A");
                }
            }
            catch (FormulaParseException e)
            {
                var errorMsg = EnumAppServ.GetAttribute<DescriptionAttribute>(e.Result, typeof(ServerResultType)).First().Description;
                Assert.That(false, string.Concat(success, " | ",errorMsg));
            }
        }
    }
}